<?php

class WhiteRabbit
{
    public function findMedianLetterInFile($filePath)
    {
        return array("letter"=>$this->findMedianLetter($this->parseFile($filePath),$occurrences),"count"=>$occurrences);
    }

    /**
     * Parse the input file for letters.
     * @param $filePath
     */
    private function parseFile ($filePath)
    {
        // Construct array(array("letter" => letter, "occurance" =>)) for letters a -z
        //initial occurance = 0
        $i = 0;
        $lettersOccurence  = array();
         for ($x = ord("a"); $x <= ord("z"); $x++) {
            $lettersOccurence[$i] =  array("letter" => chr($x), "occurance" => 0);
            $i++;
         }
        
         //open file
         $fh = fopen($filePath, "r");
        


        if ( $fh ) {
            //parse each line
          while ( !feof($fh) ) {
            $line = fgets($fh);
            //Make a char array out of lines
            $lineArr = str_split($line);

            //parse each element of char array
            for ($i = 0; $i <sizeof($lineArr); $i++) {
                $character = $lineArr[$i];
                //increase occurance by 1 every time the char is met
                if (ord($character) >= ord("a") && ord($character) <= ord("z")){
                    
                    $lettersOccurence[ord($character)-ord("a")]["occurance"]++;}
                //if char is upperCase, convert to lowercase and increase occurance by 1 
                else if (ord($character) >= ord("A") && ord($character) <= ord("Z")){
                    $lettersOccurence[ord(strtolower($character))-ord("a")]["occurance"]++;
                }
                
            }
          }
          //close file
          fclose($fh);
        }
        
        //sort the array of letter occurances by occurance in descending order
        $letter = array_column($lettersOccurence, 'letter');
        $occurance = array_column($lettersOccurence, 'occurance');
        array_multisort($occurance, SORT_DESC, $lettersOccurence);
    
        //return the array
        return $lettersOccurence;
    }

    /**
     * Return the letter whose occurrences are the median.
     * @param $parsedFile
     * @param $occurrences
     */
    private function findMedianLetter($parsedFile, &$occurrences)
    {
        // find the median index of array
        $medianIndex = (int) sizeof($parsedFile)/2;

        //the occurance of the median letter
        $occurrences = $parsedFile[$medianIndex]["occurance"];
        return ($parsedFile[$medianIndex]["letter"]);

    }

    
}
