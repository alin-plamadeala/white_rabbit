<?php

namespace test;

require_once(__DIR__ . "/../src/WhiteRabbit3.php");

use PHPUnit_Framework_TestCase;
use WhiteRabbit3;

class WhiteRabbit3Test extends PHPUnit_Framework_TestCase
{
    /** @var WhiteRabbit3 */
    private $whiteRabbit3;

    public function setUp()
    {
        parent::setUp();
        $this->whiteRabbit3 = new WhiteRabbit3();

    }

    //SECTION FILE !
    /**
     * @dataProvider multiplyProvider
     */
    public function testMultiply($expected, $amount, $multiplier){
        $this->assertEquals($expected, $this->whiteRabbit3->multiplyBy($amount, $multiplier));
    }

    public function multiplyProvider(){
        return array(
            '2*2' => array(4, 2, 2),
            '3*2' => array(6, 3, 2),
            'ammount = 7' => array(14, 7, 2),
            'ammount = 7_' => array(21, 7, 3),
            '6.51 < ammount < 7' => array(20, 6.6, 3),
//The test always fails when ammount parameter = 7
//and always rounds down when 6.51 < ammount < 7
        );
    }
}
